# Grid World: Harry & Sally
This application is a demo of how 2APL agents can interact in a virtual environment. The environment in question is a Grid World, a cell in which can contain an agent, a bomb, a bomb trap or a wall. The agents should scout the environment for bombs and, when found, carry bombs to a bomb trap to remove it from the environment, all the while moving around obstacles such as walls and other agents.

In this demo application, two (types of) agents are instantiated: Harry and Sally. Sally walks across the environment looking for the locations of bombs and bomb traps, informing Harry of those locations when found. When receiving a newly scouted location, Harry updates its belief base and sets out to move to the closest bomb location, pick up the bomb there and carry it to a bomb trap location, where it drops the bomb to remove it from the environment.

The agent code is extensively documented. Please start at the `Main.java` class to see how agents are isntantiated in a platform and placed in the environment. Comments in the code hint what other classes to inspect to understand how the agents work.

## Parameters
The environment is instantiated as a 50x50 cells grid with one Harry, one Sally, who wait 120ms after every step they take by default.
The `Main` class allows changing these parameters:
1. `N_HARRIES` and `N_SALLIES` specify how many Harry and Sally agents resp. are instantiated in the environment
2. `ENV_WIDTH` and `ENV_HEIGHT` allow changing the initial Grid World width and height respectively
3. `INSTANTIATE_ENVIRONMENT_HEADLESS` allows running the simulation in headless mode by setting this variable to false. This will not create the view 
4. `AFTER_MOVE_DELAY` specifies the number of milliseconds agents wait after each succesful step. This allows you to see the agents move. This delay can be changed (or set to `0`), which may be useful if more agents are instantiated

Three example environments are provided in the root directory of this repository (which will automatically resize the Grid World to 50x50). These can be loaded through the options menu after the simulation has started. 
## Excersises
The agents in the system are quite simple. Sally moves at random across the grid to find bombs and bomb traps, and informs any agent who will listen of their locations. Meanwhile, none of the agents take obstacles into account when planning a route; they just try to move towards their goal one step at the time. Lastly, when multiple Harry's exist in the environment, they do not coordinate, but rather all try to remove the same bombs.

These shortcomings hint at improvements that can be made to the agents to learn programming in 2APL:

1) Make the `SearchBlockworldPlan` more intelligent. Sally could keep track of previously visited locations in here belief base (`SallyContext`) and use those beliefs to chose the next point in the map to go to. She could even use consistent scouting patterns
2) Agents currently attempt to take a step closer towards their goal (see `GoToPlan`). If that step fails, because their path is blocked, they simply drop the `GoToGoal`, move to a random other position, and then continue with their original plans. With more sophisticated path planning, taking obstacles into account, the `GoToPlan` is less likely to fail and agents can move more efficiently. Moreover, plan recovery can take into account whether the path is blocked by another agent (that agent can be asked to make way) or a wall, which needs navigating around.
3) If multiple Harry's exist in the environment, they could communicate which bombs they could negotiate with other Harry's about which bombs will be handled by whom. Further more, they could inform each other (as well as other Sally's, whic is already happening) when a bomb is removed, so other Harry's won't try to clear a bomb that is already removed. 

# Installation
Requires Java 11 or higher
## IDE
The project can be imported as a Maven project in any supporting IDE. The 2APL library needs to be installed using Maven beforehand, however, as it is not in the Maven repositories. Using a UNIX terminal with Java, Git, and Maven installed, run the commands in the first box in the following section (installing the `net2apl` repository).

After 2APL is installed, import the project as a Maven project in your supporting IDE, run the Maven installer in said IDE and run `Main.java`.

## Pure maven
First make sure 2APL is installed as a Maven repository on your machine:

```bash
$ git clone https://bitbucket.org/goldenagents/net2apl.git
$ cd net2apl
$ mvn install
```

Now the GridWorld can be installed, by cloning this repository and installing with Maven using the string of commands below, or load the project in your favorite IDE with Maven support.

```bash
$ cd ../
$ git clone https://bitbucket.org/goldenagents/grid-world.git
$ cd grid-world
$ mvn install
```

## 
The project can be run without Maven, but 2APL needs to be added as a source of your project. Since 2APL is setup as a Maven project as well, the easiest way is to clone both repositories and create a simlink in the appropriate location. The following commands assume you will clone both projects in your home directory:

```bash
cd ~
git clone https://bitbucket.org/goldenagents/grid-world.git
https://bitbucket.org/goldenagents/net2apl.git
cd ~/grid-world/src/main/java/
ln -sv ~/net2apl/src/org
```

Now compile `Main.java` in the directory the previous commands left you in, or import the entire
project as a normal Java project in your (non-Maven supporting) IDE.
