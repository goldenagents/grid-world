package environment;

import environment.event.EnvironmentListener;
import environment.event.ObjectStateListener;
import environment.objects.*;
import environment.view.AgentColor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.ImageProducer;
import java.util.*;
import java.util.List;

public class EnvironmentView extends JPanel implements Observer, EnvironmentListener {

    private static final long serialVersionUID = -6981513623481400558L;

    private Environment environment;

    private Image imgStone = null;
    private Image imgBomb = null;
    private Image imgTrap = null;

    public Signal signalSelectionChanged = new Signal( "Selected agent changed" );

    private AgentObject selectedAgent = null;

    public MouseTool tool;

    public EnvironmentView(Environment env) {
        this.environment = env;

        imgStone = tryGetImage("images/stone.gif");
        imgBomb = tryGetImage("images/bomb.gif");
        imgTrap = tryGetImage("images/trap.gif");

        AgentStateConsumer asc = new AgentStateConsumer();
        BombStateConsumer bsc = new BombStateConsumer();
        TrapStateConsumer tsc = new TrapStateConsumer();
        WallStateConsumer wsc = new WallStateConsumer();
        environment.addAgentStateChangeListener(asc);
        environment.addBombStateChangeListener(bsc);
        environment.addTrapStateChangeListener(tsc);
        environment.addWallStateChangeListener(wsc);

        // track mouse events
        tool = new MouseTool(environment);
        addMouseListener( tool );
        addMouseMotionListener( tool );
    }

    private Image tryGetImage(String path) {
        Image image = null;
        try {
            image = createImage((ImageProducer)(this.getClass().getClassLoader().getResource(path)).getContent());
        } catch(Exception xEx) {
            xEx.printStackTrace();
        }
        return image;
    }

    @Override
    public void senseRangeChanged(int oldRange, int newRange) {
        EnvironmentView.this.repaint();
    }

    @Override
    public void sizeChanged(Dimension newSize, Dimension oldSize) {
        EnvironmentView.this.repaint();
    }

    @Override
    public void trapPositionChanged(BombTrap trap) {
        EnvironmentView.this.repaint();
    }

    @Override
    public void bombTrapped(BombObject bomb, AgentObject agent) {
        EnvironmentView.this.repaint();
    }

    private List<Observer> observers = new ArrayList<>();

    public boolean addChangeListener(Observer observer) {
        return this.observers.add(observer);
    }

    public boolean removeChangeListener(Observer observer) {
        return this.observers.remove(observer);
    }

    private void notifyChange() {
        this.observers.forEach(x -> x.update(new Observable(), null));
    }

    protected void paintComponent( Graphics g ) {
        super.paintComponent( g );

        notifyChange();

        final int cw = getWidth() / environment.getWidth();
        final int ch = getHeight() / environment.getHeight();
        final int cellFractionW = (int) ((double) cw / 2);
        final int cellFractionH = (int) ((double) ch / 2);

        // clear
        g.setColor( Color.white );
        g.fillRect( 0, 0, getWidth(), getHeight() );

        // draw grid contents + lines
        for(int x = 0; x < environment.getWidth(); x++ ) {
            for (int y = 0; y < environment.getHeight(); y++) {
                final Point p = new Point(x, y);

                Image img = null;
                Color c = null;
                if (environment.isStone(p)) {
                    img = this.imgStone;
                    c = Color.blue;
                } else if (environment.isTrap(p)) {
                    img = this.imgTrap;
                    c = Color.red;
                } else if (environment.isBomb(p)) {
                    img = this.imgBomb;
                    c = Color.orange;
                }

                if (img != null) {
                    g.drawImage(img, p.x * cw + 1, p.y * ch + 1, cw - 1, ch - 1, this);
                } else if (c != null) {
                    g.setColor(c);
                    g.fillRect(p.x * cw, p.y * ch, cw, ch);
                }

                // draw gridline
                g.setColor(Color.gray);
                g.drawRect( p.x * cw, p.y * ch, cw, ch);
            }
        }

        // Draw agents in the grid
        Iterator<AgentObject> a = new ArrayList<>(environment.getGridWorldAgents()).iterator();
            while (a.hasNext()) {
                final AgentObject agent = a.next();

                // draw agent position
                final Point p = agent.getPosition();
                final int x = (int) ((double) p.x * cw);
                final int y = (int) ((double) p.y * ch);
                final AgentColor color = agent.equals(selectedAgent) ? AgentColor.ARMY : agent.getColor();

                if (agent.getCarryingBomb() != null) {
                    if (color.getImageHolding() != null) {
                        g.drawImage(color.getImageHolding(), x + 1, y + 1, (cw - 1), (ch - 1), this);
                    } else {
                        g.setColor((agent != selectedAgent) ? Color.green : Color.green.darker());
                        g.fillRect(x + 1, y + 1, (int) (cw - 1), (int) (ch - 1));
                        g.setColor(Color.black);
                        g.drawRect(x + 2, y + 2, 3, 3);
                    }
                } else {
                    if (color.getImageAgent() != null) {
                        g.drawImage(color.getImageAgent(), x + 1, y + 1, (cw - 1), (ch - 1), this);
                    } else {
                        g.setColor((agent != selectedAgent) ? Color.green : Color.green.darker());
                        g.fillRect(x + 1, y + 1, (int) (cw - 1), (int) (ch - 1));
                    }
                }

                // draw sensing range
                final int rw = (int) ((double) environment.getSenseRange() * cw);
                final int rh = (int) ((double) environment.getSenseRange() * ch);
                g.setColor(Color.blue);
                g.drawOval(x - rw, y - rh, rw * 2, rh * 2);

                if(this.tool.paintTargets && (selectedAgent == null || selectedAgent.equals(agent))) {
                    // Draw target destination
                    final Point destination = agent.getAgent().getCurrentTargetDestination();
                    if (destination != null) {
                        Color c = agent.getColor().getColor();
                        Color c1 = new Color(c.getRed(), c.getGreen(), c.getBlue(), 120);
                        g.setColor(c1);
                        int targetX = (int) ((double) destination.x * cw);
                        int targetY = (int) ((double) destination.y * ch);

                        g.fillOval(targetX + cellFractionW / 2, targetY + cellFractionH / 2, cw - cellFractionW, ch - cellFractionH);
                    }
                }
            }
    }

    public void update(Observable o, Object arg ) {
        repaint();
        // Changed SA: we need some sort of event to indicate to our window we want
        // it to update its screen
        //	signalRefresh.emit();
    }

    public AgentObject getSelectedAgent() {
        return selectedAgent;
    }

    public Environment getEnv() {
        return environment;
    }

    public class MouseTool implements MouseListener, MouseMotionListener {
        Environment environment;

        public static final int STATE_SELECT = 0;

        public static final int STATE_REMOVE = 1;

        public static final int STATE_ADDBOMB = 2;

        public static final int STATE_ADDWALL = 3;

        public static final int STATE_ADDTRAP = 4;

        public int state = STATE_SELECT;

        public boolean paintTargets = false;

        MouseTool(Environment env) {
            environment = env;
        }

        Point toEnv(MouseEvent e) {
            final double cw = getWidth() / (double) environment.getWidth();
            final double ch = getHeight() / (double) environment.getHeight();
            final int cx = (int) ((double) e.getX() / cw);
            final int cy = (int) ((double) e.getY() / ch);
            return new Point( cx, cy );
        }

        void drag(MouseEvent e)
        {
            final Point p = toEnv( e );

            // Changed SA: Java is the best option to keep things slow,
            // to slow things down a bit more we accessively use exceptions.
            // But we don't want to see them all the time!
            try
            {
                // don't draw on agents
                if( environment.isAgent( p ))
                    return;

                switch(state) {
                    // add bombs, removing if needed
                    case STATE_ADDBOMB:
                        // Changed SA:
                        if( environment.isStone( p ))
                            environment.removeStone( p );

                        if( environment.isTrap( p ))
                            environment.removeTrap( p );

                        environment.addBomb( p );
                        break;

                    // add walls, removing if needed
                    case STATE_ADDWALL:
                        if( environment.isBomb( p ) )
                            environment.removeBomb( p );

                        if( environment.isTrap( p ) )
                            environment.removeTrap( p );

                        environment.addStone( p );
                        break;

                    // add traps, removing if needed
                    case STATE_ADDTRAP:
                        if( environment.isBomb( p ) )
                            environment.removeBomb( p );

                        // Changed SA:
                        if( environment.isStone( p ) )
                            environment.removeStone( p );

                        environment.addTrap( p );
                        break;

                    // removeAgent all
                    case STATE_REMOVE:
                        // Changed SA:
                        if( environment.isStone( p ) )
                            environment.removeStone( p );

                        if( environment.isBomb( p )  )
                            environment.removeBomb( p );

                        if( environment.isTrap( p ) )
                            environment.removeTrap( p );
                        break;
                }
            }
            catch (Exception error){ /* Ignore this*/}
        }

        // Changed SA:
        void start(MouseEvent e) {
            final Point p = toEnv( e );

            try {
                if (state == STATE_SELECT) {	// Try to select an agent
                    // if it's an agent, change selected agent
                    final AgentObject a = environment.getAgentAtPoint(p);
                    if (a != null)
                    {
                        selectedAgent = (a != selectedAgent) ? a : null;
                        signalSelectionChanged.emit();
                        repaint();
                    }
                    System.out.println("Selected agent is now " + selectedAgent);
                } else {
                    // Place the selected object (or erase when chosen)
                    drag(e);
                }
            } catch( Exception error ) { /* ignore error */ }
        }

        public void mouseDragged( MouseEvent arg0 ) {
            drag( arg0 );
        }
        public void mousePressed( MouseEvent arg0 ) {
            start( arg0 );
        }

        public void mouseClicked( MouseEvent arg0 ) { }
        public void mouseEntered( MouseEvent arg0 ) { }
        public void mouseExited( MouseEvent arg0 ) { }
        public void mouseReleased( MouseEvent arg0 ) { }
        public void mouseMoved( MouseEvent arg0 ) { }
    }

    private class AgentStateConsumer implements ObjectStateListener<AgentObject> {

        @Override
        public void added(AgentObject positionedObject) {
            EnvironmentView.this.repaint();
        }

        @Override
        public void removed(AgentObject positionedObject) {
            EnvironmentView.this.repaint();
        }
    }

    private class BombStateConsumer implements ObjectStateListener<BombObject> {

        @Override
        public void added(BombObject positionedObject) {
            EnvironmentView.this.repaint();
        }

        @Override
        public void removed(BombObject positionedObject) {
            EnvironmentView.this.repaint();
        }
    }

    private class TrapStateConsumer implements ObjectStateListener<BombTrap> {

        @Override
        public void added(BombTrap positionedObject) {
            EnvironmentView.this.repaint();
        }

        @Override
        public void removed(BombTrap positionedObject) {
            EnvironmentView.this.repaint();
        }
    }

    private class WallStateConsumer implements ObjectStateListener<WallObject> {

        @Override
        public void added(WallObject positionedObject) {
            EnvironmentView.this.repaint();
        }

        @Override
        public void removed(WallObject positionedObject) {
            EnvironmentView.this.repaint();
        }
    }

}
