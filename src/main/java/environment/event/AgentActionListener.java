package environment.event;

import environment.objects.AgentObject;
import environment.objects.BombObject;

import java.awt.*;

public interface AgentActionListener {

    void attemptBombPickup(BombObject bomb, AgentObject agent);

    void attemptBombDrop(BombObject bomb, AgentObject agent);

    void bombPickedUp(BombObject bomb, AgentObject agent);

    void bombDropped(BombObject bomb, AgentObject agent);


    void positionChanged(AgentObject positionedObject, Point oldPosition);
}
