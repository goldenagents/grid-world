package environment.event;

import environment.objects.AgentObject;
import environment.objects.BombObject;
import environment.objects.BombTrap;

import java.awt.*;

public interface EnvironmentListener {

    void senseRangeChanged(int oldRange, int newRange);

    void sizeChanged(Dimension newSize, Dimension oldSize);

    void trapPositionChanged(BombTrap trap);

    void bombTrapped(BombObject bomb, AgentObject agent);
}
