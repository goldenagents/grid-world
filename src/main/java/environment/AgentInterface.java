package environment;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public interface AgentInterface {

    /**
     * The agent interface should generate an identifier that will be used by both the agents to identify
     * them with the environment, as by the environment to signal events to an agent
     *
     * @return An object used as an identifier
     */
    Object getAgentIdentifier();

    /**
     * A human-readable name for the agent
     * @return  String
     */
    @Nullable String getAgentName();

    /**
     * A list of active goals, in Predicate-logic notation (e.g. "goto(X,Y)"). Used to show belief base of this agent in
     * the GUI
     * @return List of active goals of the agent
     */
    @NotNull List<String> getPursuingGoals();

    /**
     * A list of beliefs in Predicate-logic notation (e.g. "bomb(X,Y)"). Used to show belief base of this agent in
     * the GUI
     * @return  List of beliefs of the agent
     */
    @NotNull List<String> getBeliefs();

    /**
     * Returns the cell coordinates in the GridWorld that the agent is currently (trying to) move towards
     * @return Point on GridWorld
     */
    @Nullable Point getCurrentTargetDestination();

    /**
     * Get an ordered list of GridWorld coordinates representing the planned path the agent will take towards its
     * destination
     * @return List of adjoining GridWorld coordinates
     */
    @Nullable ArrayList<Point> getCurrentPlannedPath();

    // TODO maybe add events from the environment here?
}
