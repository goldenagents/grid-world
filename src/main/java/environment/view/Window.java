package environment.view;

import environment.Environment;
import environment.EnvironmentView;
import environment.Statistics;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Window extends JFrame {
    private static final long serialVersionUID = -462965463955994316L;

    private boolean done = false;
    private boolean init = false;
    private File lastFile = null;

    private JToolBar toolbar = null;
    private ArrayList<JToggleButton> editOptions = new ArrayList<>();

    public Window( final Environment environment ){
        super( "Grid World" );

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        // create clear menuitem + action
        JMenuItem clear = new JMenuItem( "Clear environment" );
        clear.addActionListener(e -> {
            // popup confirmation dialog
            final int rv = JOptionPane.showConfirmDialog( Window.this,
                    "Are you sure you want to clear the environment",
                    "Confirm clear", JOptionPane.YES_NO_OPTION );

            // abort unless YES is pressed
            if( rv != JOptionPane.YES_OPTION )
                return;

            environment.clear();
        });

        // create revert menuitem + action
        JMenuItem revert = new JMenuItem( "Revert to saved" );
        revert.addActionListener(e -> {
            // see if we can revert at all
            if( lastFile == null ) {
                JOptionPane
                        .showMessageDialog(
                                Window.this,
                                "You did not load or save this environment yet.\nPlease load or save first",
                                "Nothing to revert to",
                                JOptionPane.ERROR_MESSAGE );
                return;
            }

            // popup confirmation dialog
            final int rv = JOptionPane.showConfirmDialog( Window.this,
                    "Are you sure you want revert to "
                            + lastFile.getPath(),
                    "Confirm revert to saved", JOptionPane.YES_NO_OPTION );

            // abort unless YES is pressed
            if( rv != JOptionPane.YES_OPTION )
                return;

            try {
                environment.clear();
                final FileInputStream stream = new FileInputStream(lastFile);
                environment.load( stream );
            }
            catch( Exception ex ) {
                System.out.println( "Loading failed! " + ex );
            }
        });

        // create 'load' button + action
        JMenuItem load = new JMenuItem( "Load from File" );
        load.addActionListener(e -> {
            try {
                final File cwd = new File( "." );
                final JFileChooser fc = new JFileChooser( cwd );
                final int rv = fc.showOpenDialog( Window.this );

                // cancel pressed
                if( rv != JFileChooser.APPROVE_OPTION )
                    return;

                final File file = fc.getSelectedFile();
                final FileInputStream stream = new FileInputStream( file );
                environment.load( stream );
                lastFile = file;
            }
            catch( Exception ex ) {
                System.out.println( "Loading failed! " + ex );
            }
        });

        JMenuItem save = new JMenuItem( "Save to File" );
        save.addActionListener(e -> {
            try {
                final File cwd = new File( "." );
                final JFileChooser fc = new JFileChooser( cwd );
                final int rv = fc.showSaveDialog( Window.this );

                // cancel pressed
                if( rv != JFileChooser.APPROVE_OPTION )
                    return;

                final File file = fc.getSelectedFile();
                final FileOutputStream stream = new FileOutputStream( file );
                environment.save( stream );
                lastFile = file;
            }
            catch( Exception ex ) {
                System.out.println( "Saving failed! " + ex );
            }
        });

        JMenuItem resize = new JMenuItem( "Environment Size" );
        resize.addActionListener(e -> {
            // show dialog
            String size = (String) JOptionPane.showInputDialog(
                    Window.this,
                    "Resize environment (X * Y) (X, Y) (X x Y) (X Y)",
                    "Resize environment", JOptionPane.PLAIN_MESSAGE, null, null, environment
                            .getWidth()
                            + " * " + environment.getHeight() );

            // if a string was returned, parse and set size
            if( (size != null) && (size.length() > 0) ) {
                StringTokenizer st = new StringTokenizer( size, "*x, " );

                if( !st.hasMoreTokens() )
                    return;
                String x = st.nextToken();

                if( !st.hasMoreTokens() )
                    return;
                String y = st.nextToken();

                try {
                    Dimension d = new Dimension( Integer.parseInt( x ),
                            Integer.parseInt( y ) );

                    environment.setSize( d );
                }
                catch( NumberFormatException e1 ) {
                    JOptionPane
                            .showMessageDialog(
                                    Window.this,
                                    "Invalid number format, it should be one of (X * Y) (X, Y) (X x Y) (X Y)",
                                    "Error parsing size",
                                    JOptionPane.ERROR_MESSAGE );
                }
            }

            Window.this.repaint();
        });

        JMenuItem setid = new JMenuItem( "Default APLIdentifier" );
        setid.addActionListener(e -> {
            // show dialog
            String objType = ((String) JOptionPane.showInputDialog(
                    Window.this,
                    "Enter default identifier for new bombs/traps (all char, first char in lowercase)",
                    "Set Default Object APLIdentifier", JOptionPane.PLAIN_MESSAGE, null, null, environment.getObjType() ));
            // -- validate objType --
            if(objType == null) return;
            boolean invalid = false;
            for(int i = 0; i < objType.length(); i++) {
                char ichar = objType.charAt(i);
                if(!Character.isLetter(ichar) || (i ==0 && !Character.isLowerCase(ichar))) {
                    invalid = true;
                    break;
                }
            }

            if(objType.length() == 0 || invalid) {
                JOptionPane.showMessageDialog(Window.this,
                        "The object type identifier must be an all-character string with the first character in lowercase",
                        "Invalid object type",
                        JOptionPane.ERROR_MESSAGE );
                return;
            }

            environment.setObjType(objType);
        });

        JMenuItem senserange = new JMenuItem( "Sensor Range" );
        senserange.addActionListener(e -> {
            String range = (String) JOptionPane.showInputDialog(
                    Window.this, "Set agent sensor range in cells",
                    "Set Sensor Range in Cells", JOptionPane.PLAIN_MESSAGE, null, null,
                    Integer.toString( environment.getSenseRange() ) );

            // If a string was returned, parse and set range
            if( (range != null) && (range.length() > 0) )
                environment.setSenseRange( Integer.parseInt( range ) );
        });

        JMenuItem about = new JMenuItem( "About Grid World" );
        about.addActionListener(e -> {
            // show dialog
            JOptionPane.showMessageDialog(Window.this,
                    "<html>" +
                            "<h3>GridWorld for 2AMP</h3>" +
                            "<p><a href=\"http://www2.projects.science.uu.nl/Net2APL\" title=\"2APL Official Website\">http://www2.projects.science.uu.nl/Net2APL</a></p>" +
                            "<p><br/><small>Developed by:</small></p>" +
                            "<p>The 2APL Development Group<br/>" +
                            "  <center><small>and</small><br/></center>" +
                            "  The Golden Agents Group,<br/>" +
                            "  Intelligent Systems,<br>" +
                            "  <a href=\"https://www.uu.nl/en\" title=\"Utrecht University\">Utrecht University</a>, The Netherlands" +
                            "</p>",
                    "About GridWorld",
                    JOptionPane.INFORMATION_MESSAGE );
        });

        JMenu world = new JMenu( "World" );
        world.add( load );
        world.add( save );
        world.add( revert );
        world.add( clear );

        JMenu properties = new JMenu( "Properties" );
        properties.add( resize );
        properties.add( senserange );
        properties.add( setid );

        JMenu help = new JMenu( "Help" );
        help.add( about );

        JMenuBar menubar = new JMenuBar();
        menubar.add( world );
        menubar.add( properties );
        menubar.add( help );

        // create window
        getContentPane().setLayout( new BorderLayout() );

        final EnvironmentView envView = new EnvironmentView( environment );
//        final Statistics stats = new Statistics( envView );
        final Statistics stats = new Statistics( environment, envView);
        final JComponent statsView = new JScrollPane( new JTable( stats ) );

        final JSplitPane sp = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT,
                envView, statsView );
        sp.setOneTouchExpandable( true );
        sp.setDividerLocation( 650 );
        sp.setResizeWeight(1);
        getContentPane().add( sp, BorderLayout.CENTER );
        setJMenuBar( menubar );

        // The toolbar
        toolbar = new JToolBar();
        toolbar.setFloatable(false);
        addButton("info.gif", "Select agent", EnvironmentView.MouseTool.STATE_SELECT, envView.tool).setSelected(true);
        addButton("bomb.gif", "Place bombs", EnvironmentView.MouseTool.STATE_ADDBOMB, envView.tool);
        addButton("stone.gif", "Place walls", EnvironmentView.MouseTool.STATE_ADDWALL, envView.tool);
        addButton("trap.gif", "Place traps", EnvironmentView.MouseTool.STATE_ADDTRAP, envView.tool);
        addButton("eraser.gif", "Erase objects", EnvironmentView.MouseTool.STATE_REMOVE, envView.tool);
        addShowTargetsButton("target.png", "Toggle agent destinations", envView.tool);
        getContentPane().add(toolbar, BorderLayout.NORTH);


        // pack();
        setSize( 1000, 650 );
        setVisible( true );
    }

    // / the first call to this method will display the
    // / edit window and block until user has finished editing
    public synchronized void init() {
        if(init)
            return;

        while( !done) {
            try {
                wait();
            } catch( InterruptedException e ) {
            }
        }

        init = true;
    }

    // / call this method to end the editing session. This function
    // / is called from a JButton.
    protected synchronized void done() {
        done = true;
        notifyAll();
    }


    public JToggleButton addButton(String sImage, String tooltip, final int nState, final EnvironmentView.MouseTool tool)
    {
        final JToggleButton button = new JToggleButton(makeIcon(sImage));
        button.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                tool.state = nState;

                for (JToggleButton cmdButton : editOptions)
                {
                    if (button != cmdButton)
                    {
                        cmdButton.setSelected(false);
                    }
                }
            }
        });
        button.setToolTipText(tooltip);
        toolbar.add(button);
        editOptions.add(button);

        return button;
    }

    public JToggleButton addShowTargetsButton(String sImage, String tooltip, final EnvironmentView.MouseTool tool) {
        final JToggleButton button = new JToggleButton(makeIcon(sImage));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                tool.paintTargets = !tool.paintTargets;
                button.setSelected(tool.paintTargets);
            }
        });
        button.setToolTipText(tooltip);
        toolbar.add(button);

        return button;
    }

    private ImageIcon makeIcon(String sImage)
    {
        sImage	= "images/toolbar/"+sImage;
        return new ImageIcon(this.getClass().getClassLoader().getResource(sImage));
    }
}
