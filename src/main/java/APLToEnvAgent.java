import agent.person.GoToGoal;
import agent.person.PersonContext;
import environment.AgentInterface;
import org.jetbrains.annotations.Nullable;
import org.uu.nl.net2apl.core.agent.Agent;
import org.uu.nl.net2apl.core.agent.Goal;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class APLToEnvAgent implements AgentInterface {

    private final Agent agent;
    private String humanName;

    public APLToEnvAgent(Agent agent) {
        this.agent = agent;
        this.humanName = agent.getAID().getShortLocalName();
    }

    public APLToEnvAgent(Agent agent, String humanName) {
        this.agent = agent;
        this.humanName = humanName;
    }

    /**
     * The agent interface should generate an identifier that will be used by both the agents to identify
     * them with the environment, as by the environment to signal events to an agent
     *
     * @return An object used as an identifier
     */
    @Override
    public Object getAgentIdentifier() {
        return this.agent.getAID();
    }

    /**
     * A human-readable name for the agent
     *
     * @return String
     */
    @Override
    public @Nullable String getAgentName() {
        return this.humanName;
    }

    /**
     * A list of active goals, in Predicate-logic notation (e.g. "goto(X,Y)"). Used to show belief base of this agent in
     * the GUI
     *
     * @return List of active goals of the agent
     */
    @Override
    public List<String> getPursuingGoals() {
        try {
            synchronized (this.agent) {
                return this.agent.getGoals().stream().map(Goal::toString).collect(Collectors.toList());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    /**
     * A list of beliefs in Predicate-logic notation (e.g. "bomb(X,Y)"). Used to show belief base of this agent in
     * the GUI
     *
     * @return List of beliefs of the agent
     */
    @Override
    public List<String> getBeliefs() {
        synchronized (this.agent) {
            return this.agent.getContext(PersonContext.class).getBeliefs();
        }
    }

    /**
     * Returns the cell coordinates in the GridWorld that the agent is currently (trying to) move towards
     *
     * @return Point on GridWorld
     */
    @Override
    public @Nullable Point getCurrentTargetDestination() {
        synchronized (this.agent) {
            var goals = this.agent.getGoals();
            for(Goal g : goals) {
                if(g instanceof GoToGoal) {
                    return ((GoToGoal) g).getDestination();
                }
            }
        }
        return null;
    }

    /**
     * Get an ordered list of GridWorld coordinates representing the planned path the agent will take towards its
     * destination
     *
     * @return List of adjoining GridWorld coordinates
     */
    @Override
    public @Nullable ArrayList<Point> getCurrentPlannedPath() {
        return new ArrayList<>();
    }
}
