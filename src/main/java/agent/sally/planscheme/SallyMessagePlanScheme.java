package agent.sally.planscheme;

import agent.sally.plan.SallyProcessMessagePlan;
import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Trigger;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanScheme;

/**
 * The message plan scheme matches incoming messages to a plan to process the message. In this case, all incoming
 * messages are processed by the same {@code SallyProcessMessagePlan}, since there is only a very limited number of
 * expected types of messages.
 *
 * More complicated Message Plan Schemes could e.g. use the message performative, or even content, to trigger an
 * appropriate plan.
 */
public class SallyMessagePlanScheme implements PlanScheme {

    @Override
    public Plan instantiate(Trigger trigger, AgentContextInterface agentContextInterface) {
        Plan plan = Plan.UNINSTANTIATED;

        if(trigger instanceof ACLMessage) {
            plan = new SallyProcessMessagePlan((ACLMessage) trigger);
        }

        return plan;
    }
}
