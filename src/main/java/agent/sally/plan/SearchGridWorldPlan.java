package agent.sally.plan;

import agent.person.GoToGoal;
import agent.sally.SallyContext;
import environment.Environment;
import environment.exceptions.ExternalActionFailedException;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.fipa.acl.Performative;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;

/**
 * A simple plan to search the grid world for bombs and bomb traps. The plan is activated through the plan scheme if
 * the {@code SearchGridWorldGoal} is active.
 *
 * The plan chooses random points on the grid to move towards, continously sensing the environment for objects. If an
 * object is encountered which Sally beliefs other agents do not yet know of, she messages the other agents with the
 * position of that object.
 */
public class SearchGridWorldPlan extends Plan {

    private SallyContext context;
    private PlanToAgentInterface planToAgentInterface;

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        try {
            this.context = planToAgentInterface.getContext(SallyContext.class);
            this.planToAgentInterface = planToAgentInterface;
            Environment env = context.getEnvironment();

            try {
                env.senseBombs(planToAgentInterface.getAgentID()).forEach(this::informBomb);
                env.senseTraps(planToAgentInterface.getAgentID()).forEach(this::informTrap);
            } catch (ExternalActionFailedException e) {
                System.err.println(e.getMessage());
            }

            Point randomPoint = new Point(
                    (int) Math.round(Math.random() * (env.getWidth() - 1)),
                    (int) Math.round(Math.random() * (env.getHeight() - 1))
            );
            GoToGoal newGoToGoal = new GoToGoal(randomPoint);

            // Make sure no new GoToGoal is adopted if one is already in the goal base. Otherwise, Sally would try to
            // move to various random points at the same time, thus getting nowhere
            GoToGoal.adoptIfNoOther(newGoToGoal, planToAgentInterface);
        } catch(Exception e) {
            e.printStackTrace();
        }

        // Unlike other plans, this plan is never marked as finished, since it should continue executing indefinitely
    }

    /**
     * Inform other agents there is a bomb somewhere
     * @param p     Position of the newly discovered bomb
     */
    private void informBomb(Point p) {
        if(this.context.addInformedBomb(p))
            inform("bombAt", p);
    }

    /**
     * Inform other agents there is a bomb trap somewhere
     * @param p     Position of the newly discovered bomb trap
     */
    private void informTrap(Point p) {
        if(this.context.addInformedTrap(p))
            inform("trapAt", p);
    }

    /**
     * Send the actual message to other agents
     * @param directive     Message directive indicating type of found object (e.g. "bombAt" or "trapAt")
     * @param p             Position of newly discovered object
     */
    private void inform(String directive, Point p) {
        ACLMessage newMessage = new ACLMessage(Performative.INFORM);
        newMessage.setSender(this.planToAgentInterface.getAgentID());
        newMessage.setContent(String.format("%s(%d,%d)",directive, p.x, p.y));
        newMessage.setReceivers(this.context.getOtherAgents());
        for (AgentID agentID : this.context.getOtherAgents()) {
            this.planToAgentInterface.sendMessage(agentID, newMessage);
        }
    }
}
