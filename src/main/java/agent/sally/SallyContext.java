package agent.sally;

import agent.person.PersonContext;
import environment.Environment;
import org.uu.nl.net2apl.core.agent.AgentID;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Sally's belief base, which can be queried by Sally for her beliefs, and updated when beliefs change.
 * It consists of a set of AgentID's of agents to inform of new bombs or traps found in the environment. Every time
 * Sally encounters a new object in the environment, she queries her beliefs for agents interested in that knowledge.
 *
 * To avoid sending the same location every time the object is encountered, Sally also stores which objects have already
 * been sent to other agents. She only sends the message if she beliefs the other agent does not know the object yet.
 */
public class SallyContext extends PersonContext {

    /**
     * Other agents in the environment, who should be informed of newly found object locations. These agents
     * are usually Harry's
     */
    private final Set<AgentID> otherAgents = new HashSet<>();

    /**
     * Positions of objects Sally beliefs are already known to the other agents
     */
    private final Set<Point> informedBombLocations = new HashSet<>();
    private final Set<Point> informedTrapLocations = new HashSet<>();

    /**
     * Default constructor for Sally's belief base
     * @param environment   A reference to the environment
     */
    public SallyContext(Environment environment) {
        super(environment);
    }

    /**
     * Add an agent in the environment that should be informed of newly discovered objects to Sally's belief base
     * @param otherAgent     AgentID of agent to inform of newly discovered objects
     */
    public void addAgent(AgentID otherAgent) {
        synchronized (this.otherAgents) {
            this.otherAgents.add(otherAgent);
        }
    }

    /**
     * Remove an agent in the environment that should be informed of newly discovered objects to Sally's belief base
     * @param otherAgent     AgentID of agent to no longer inform of newly discovered objects
     */
    public void removeAgent(AgentID otherAgent) {
        synchronized (this.otherAgents) {
            this.otherAgents.remove(otherAgent);
        }
    }

    /**
     * Get a set of AgentID's that should be informed of newly discovered objects
     * @return  A set of AgentID's
     */
    public Set<AgentID> getOtherAgents() {
        synchronized (this.otherAgents) {
            return new HashSet<>(this.otherAgents);
        }
    }

    /**
     * Add the belief that other agents know there is a bomb in a certain position
     * @param bomb  Position of the bomb
     * @return  True iff the bomb was not yet in Sally's belief base
     */
    public boolean addInformedBomb(Point bomb) {
        synchronized (this.informedBombLocations) {
            return this.informedBombLocations.add(bomb);
        }
    }

    /**
     * Add the belief that other agents know there is a bomb trap in a certain position
     * @param trap  Position of the bomb trap
     * @return  True iff the bomb trap was not yet in Sally's belief base
     */
    public boolean addInformedTrap(Point trap) {
        synchronized (this.informedTrapLocations) {
            return this.informedTrapLocations.add(trap);
        }
    }

    /**
     * Update Sally's belief to reflect that she no longer beliefs the other agents know there is a bomb at a certain
     * position. This may be caused by another agent having picked up the bomb or not being able to find it. Sally
     * should re-inform all other agents if a bomb reappears in this location.
     * @param bomb  Position where there used to be a bomb
     * @return      True if this belief update results in changed beliefs, i.e. if Sally used to belief other agents to
     *              belief there was a bomb at this position
     */
    public boolean setNoBomb(Point bomb) {
        synchronized (this.informedBombLocations) {
            return this.informedBombLocations.remove(bomb);
        }
    }

    /**
     * Update Sally's belief to reflect that she no longer beliefs the other agents know there is a bomb trap at a certain
     * position. This may be caused by another not being able to find it. Sally should re-inform all other agents if a
     * bomb trap reappears in this location.
     * @param trap  Position where there used to be a bomb trap
     * @return      True if this belief update results in changed beliefs, i.e. if Sally used to belief other agents to
     *              belief there was a bomb trap at this position
     */
    public boolean setNoTrap(Point trap) {
        synchronized (this.informedTrapLocations) {
            return this.informedTrapLocations.remove(trap);
        }
    }

    /**
     * Reset the belief base of where other agents think there might be bomb traps
     */
    public void clearTraps() {
        synchronized (this.informedTrapLocations) {
            this.informedTrapLocations.clear();
        }
    }

    /**
     * Reset the belief base of where other agents think there might be bombs
     */
    public void clearBombs() {
        synchronized (this.informedBombLocations) {
            this.informedBombLocations.clear();
        }
    }

    /**
     * A string list of Sally's belief base, used in the GUI
     * @return A string list of Sally's belief base
     */
    @Override
    public ArrayList<String> getBeliefs() {
        ArrayList<String> beliefs = super.getBeliefs();
        synchronized (this.informedBombLocations) {
            beliefs.addAll(this.informedBombLocations.stream().map(x -> String.format("informed(bomb(%d,%d))", x.x, x.y)).collect(Collectors.toList()));
        }
        synchronized (this.informedTrapLocations) {
            beliefs.addAll(this.informedTrapLocations.stream().map(x -> String.format("informed(trap(%d,%d))", x.x, x.y)).collect(Collectors.toList()));
        }
        return beliefs;
    }
}
