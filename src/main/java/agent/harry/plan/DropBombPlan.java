package agent.harry.plan;

import agent.harry.HarryContext;
import environment.exceptions.ExternalActionFailedException;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;

/**
 * A simple plan to drop a bomb that Harry is carrying. To ensure Harry functions properly, he checks his beliefs
 * for all prerequisites before actually attempting to drop the bomb.
 *
 * If the environment signals a fail for the bomb drop, Harry assumes there is no bomb here, contrary to his beliefs. He
 * updates his beliefs, and informs Sally he no longer thinks there is a bomb there (so if one reappears, Sally will
 * inform him again).
 */
public class DropBombPlan extends Plan {

    private HarryContext context;
    private PlanToAgentInterface planToAgentInterface;

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        this.context = planToAgentInterface.getContext(HarryContext.class);
        this.planToAgentInterface = planToAgentInterface;

        if(!context.isCarryingBomb()) {
            // A plan that is not explicitly marked finished will be executed in each deliberation cycle. Avoid that.
            setFinished(true);
        } else if(!isTrapPosition()) {
            // Adopt goal to go to nearest trap
            removeTrap(planToAgentInterface);
            setFinished(true);
        } else {
            // Attempt to drop the bomb
            try {
                this.context.getEnvironment().drop(this.planToAgentInterface.getAgentID());
                this.context.setCarryingBomb(false);
                setFinished(true);
            } catch (ExternalActionFailedException e) {
                // Beliefs do not align with environment
                removeTrap(planToAgentInterface);
                setFinished(true);
            }
        }
    }

    private void removeTrap(PlanToAgentInterface planToAgentInterface) {
        System.out.println("Not a trap at " + context.getCurrentLocation());
        context.getTrapLocations().remove(context.getCurrentLocation());
        planToAgentInterface.adoptPlan(new InformNoObjectPlan(InformNoObjectPlan.ObjectType.TRAP, context.getCurrentLocation()));
    }

    /**
     * The only sensing Harry is allowed to do is to check if there is a trap at the current position.
     * @return True iff there is a trap at Harry's current position
     */
    private boolean isTrapPosition() {
        if(!this.context.trapAtPosition()) {
            try {
                java.util.List<Point> sensedTraps = context.getEnvironment().senseTraps(planToAgentInterface.getAgentID());
                this.context.getTrapLocations().addAll(sensedTraps);
            } catch (ExternalActionFailedException e) {
                return false;
            }
        }

        return this.context.trapAtPosition();
    }
}
