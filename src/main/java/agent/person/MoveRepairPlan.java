package agent.person;

import environment.AgentInterface;
import environment.Environment;
import environment.exceptions.ExternalActionFailedException;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * If the {@code GoToPlan} tried to perform a move in a certain direction, which was disallowed by the environment (e.g. because
 * the path was blocked), the {@code GoToPlan} has failed. It throws a {@code MoveExecutionError}, which is adopted
 * by the agent as an Internal Trigger, meaning the plan needs to be repaired. This is a recovery plan for failed plans.
 *
 * This specific repair plan just drops the {@code GoToGoal} that failed, and adopts a new goal to move to some random
 * point on the grid. Because the {@code GoToPlan} is relatively simple, usually the original location can be reached
 * from a new point, meaning the plan is saved.
 *
 * It is left as en exercise to the reader to extend the {@code GoToPlan} with more intelligent path planning, and
 * extend this recovery plan accordingly.
 */
public class MoveRepairPlan extends Plan {

    private MoveExecutionError error;
    private ArrayList<MoveExecutionError.MOVE_DIRECTION> triedDirections = new ArrayList<>();
    private PersonContext context;
    private Environment environment;
    private AgentID agentID;

    public MoveRepairPlan(MoveExecutionError error) {
        this.error = error;
    }

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        this.context = planToAgentInterface.getContext(PersonContext.class);
        this.environment = context.getEnvironment();
        this.agentID = planToAgentInterface.getAgentID();

        dropUnattainableGoal(planToAgentInterface, true);

        setFinished(true);
    }

    private void attemptMove(MoveExecutionError.MOVE_DIRECTION direction)
            throws ExternalActionFailedException {
        delay();
        switch (direction) {
            case NORTH:
                this.environment.north(this.agentID);
                break;
            case SOUTH:
                this.environment.south(this.agentID);
                break;
            case EAST:
                this.environment.east(this.agentID);
            case WEST:
                this.environment.west(this.agentID);
        }

        this.context.setCurrentLocation(this.environment.sensePosition(this.agentID));
    }

    /**
     * Check if the destination square is not occupied by an object. If it is, this goal is unattainable and it should
     * no longer be pursued.
     *
     * @param planToAgentInterface  Plan to Agent interface
     * @return  False iff this goal is no longer attainable because the destination square is already occupied.
     *          True in all other cases
     */
    protected boolean isGoalAchievable(PlanToAgentInterface planToAgentInterface) {
        PersonContext context = planToAgentInterface.getContext(PersonContext.class);
        Environment env = context.getEnvironment();
        int senseRange = env.getSenseRange();
        if(this.error.getDestination().distance(context.getCurrentLocation()) < senseRange) {
            try {
                HashMap<AgentInterface, Point> otherAgents = env.senseAgents(planToAgentInterface.getAgentID());
                List<Point> walls = env.senseStones(planToAgentInterface.getAgentID());
                walls.addAll(otherAgents.values());

                for(Point p : walls) {
                    if(p.equals(this.error.getDestination())) {
                       return false;
                    }
                }

            } catch (ExternalActionFailedException e) {
                System.err.println(e.getMessage());
            }
        }

        return true;
    }

    /**
     * Drop the unattaible goal that failed and triggered this repair plan. If no other {@code GoToGoal} exists
     * in the agents belief base, adopt a new goal to move to a random location on the grid.
     *
     * @param planToAgentInterface PlanToAgentInterface
     * @param tryAdoptRandom       If true, a new goal will be adopted to move to some random point on the grid, if no
     *                             other {@code GoToGoal} exists in the agent's goal base.
     */
    private void dropUnattainableGoal(PlanToAgentInterface planToAgentInterface, boolean tryAdoptRandom) {
        // This goal is no longer achievable under the current circumstances. Drop.
        planToAgentInterface.getAgent().dropGoal(this.error.getFailedGoal());

        if(tryAdoptRandom) {
            // Let's move to a random location and hope that solves our problem
            Point randomPoint = new Point(
                    (int) Math.round(Math.random() * (this.environment.getWidth() - 1)),
                    (int) Math.round(Math.random() * (this.environment.getHeight() - 1))
            );
            GoToGoal newGoToGoal = new GoToGoal(randomPoint);
            GoToGoal.adoptIfNoOther(newGoToGoal, planToAgentInterface);
        }
    }

    private void delay() {
        if(GoToPlan.AFTER_MOVE_DELAY > 0) {
            try {
                TimeUnit.MILLISECONDS.sleep(GoToPlan.AFTER_MOVE_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
