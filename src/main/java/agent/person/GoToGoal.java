package agent.person;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Goal;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;

import java.awt.*;

/**
 * A goal to reach a certain coordinate on the grid world. The goal is achieved if the location of the agent is the
 * same as that destination.
 */
public class GoToGoal extends Goal {

    private boolean achieved;
    private Point destination;
    private static final Object mutex = new Object();

    /**
     * Default constructor
     * @param destination   Grid coordinates to go to as a destination
     */
    public GoToGoal(Point destination) {
        this.destination = destination;
    }

    public Point getDestination() {
        return this.destination;
    }

    @Override
    public boolean isAchieved(AgentContextInterface agentContextInterface) {
        return isAchieved(agentContextInterface.getContext(PersonContext.class));
    }

    public boolean isAchieved(PlanToAgentInterface planToAgentInterface) {
        return isAchieved(planToAgentInterface.getContext(PersonContext.class));
    }

    private boolean isAchieved(PersonContext context) {
        return this.achieved || this.destination.equals(context.getCurrentLocation());
    }

    /**
     * Explicitly set this goal as being achieved, in case it's missed by the deliberation cycle
     */
    void markAchieved() {
        this.achieved = true;
    }

    @Override
    public boolean equals(Object other) {
        if(other instanceof GoToGoal) {
            GoToGoal otherGoal = (GoToGoal) other;
            return this.destination == null && otherGoal.destination == null ||
                    this.destination.equals(otherGoal.destination);
        }

        return false;
    }

    /**
     * Check if the agent is already pursuing another goal to go to some coordinates. If so, adopting a new GoToGoal
     * would clash with that goal, meaning the agent will move very spastically. Not adopting a GoToGoal if another had
     * already been adopted avoids these spasms.
     *
     * @param planToAgentInterface  PlanToAgentInterface
     * @return  True iff there is already a GoToGoal being pursued by the agent
     */
    private static synchronized boolean hasGotoGoal(PlanToAgentInterface planToAgentInterface) {
        synchronized (planToAgentInterface.getAgent().getGoals()) {
            for (Goal g : planToAgentInterface.getAgent().getGoals()) {
                if (g instanceof GoToGoal && !((GoToGoal) g).isAchieved(planToAgentInterface))
                    return true;
            }
            return false;
        }
    }

    public static synchronized boolean adoptIfNoOther(GoToGoal goal, PlanToAgentInterface planToAgentInterface) {
        synchronized (mutex) {
            if(!hasGotoGoal(planToAgentInterface)) {
                planToAgentInterface.getAgent().adoptGoal(goal);
                return true;
            } else return false;
        }
    }

    public String toString() {
        return String.format("goto(%d,%d)", this.destination.x, this.destination.y);
    }
}
