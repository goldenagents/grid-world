package agent.person;

import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;

/**
 * Error object to signal a move in a specific direction was disallowed by the environment, which requires
 * replanning
 */
public class MoveExecutionError extends PlanExecutionError {

    private MOVE_DIRECTION failedDirection;
    private GoToGoal goal;


    public MoveExecutionError(GoToGoal goToGoal, MOVE_DIRECTION failedDirection) {
        this.failedDirection = failedDirection;
        this.goal = goToGoal;
    }

    public MOVE_DIRECTION getFailedDirection() {
        return this.failedDirection;
    }

    public Point getDestination() {
        return this.goal.getDestination();
    }

    public GoToGoal getFailedGoal() {
        return this.goal;
    }

    public enum MOVE_DIRECTION {

        NORTH, EAST, SOUTH, WEST, INVALID;

    }

}
