package agent.person;

import environment.Environment;
import org.uu.nl.net2apl.core.agent.Context;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/**
 * The Person agent is a generic agent that is able to move on the grid in the Grid World environment. Generic agents
 * can be extended to specific agents (e.g. Harry and Sally), by extending the context to include the specific agent's
 * beliefs, and adding new Trigger Interceptors (e.g. GoalPlanScheme, MessagePlanScheme, etc...) to trigger new plans
 * based on new triggers.
 *
 * This specific context stores general beliefs about the agents position in the environment, allowing it to make
 * decisions on how to move. The plans and goals of this agent also relate to moving through the environment. Since
 * these beliefs, plans, and actions are common for all agents operating in the Grid World, it has been implemented
 * generally, and been extended for specific agents.
 */
public class PersonContext implements Context {

    protected Environment environment;
    protected Point currentLocation;

    /**
     * Default constructor
     *
     * @param environment A reference to the Grid World environment
     */
    public PersonContext(Environment environment) {
        this.environment = environment;
    }

    /**
     * Advanced constructor
     * @param environment           A reference to the Grid World environment
     * @param startingPosition      The position in the grid this agent is spawned at
     */
    public PersonContext(Environment environment, Point startingPosition) {
        this.environment = environment;
        this.currentLocation = startingPosition;
    }

    /**
     * Get a reference of the Grid World environment
     * @return  A reference of the Grid World environment this agent is based in
     */
    public synchronized Environment getEnvironment() {
        return this.environment;
    }

    /**
     * Setter for environment
     * @param environment   The new Grid World environment this agent is based in
     */
    public synchronized void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    /**
     * Get the current coordinates of the agent in the Grid World, according to its beliefs
     * @return  Agent's believed current coordinates in the Grid World
     */
    public synchronized Point getCurrentLocation() {
        return (Point) currentLocation.clone();
    }

    /**
     * Update the coordinates of the agent in its belief base
     * @param currentLocation   New coordinates
     */
    public synchronized void setCurrentLocation(Point currentLocation) {
        this.currentLocation = currentLocation;
    }

    /**
     * Test if there is an item in the belief base which is at the same position as the agent's current believed
     * position
     *
     * @param itemList Belief base to test
     * @return  True iff an item in the belief base is believed to be at the same position as the agent testing it
     */
    protected synchronized boolean itemAtPosition(Set<Point> itemList) {
        for(Point p : itemList) {
            if(p.equals(this.currentLocation)) return true;
        }
        return false;
    }

    /**
     * Find the closest point to the currentLocation in a list of Points.
     *
     * Note: If planning becomes a consideration, i.e. point with shortest
     * path, rather than closest point, it would be neater to create a plan
     * for it, as it becomes deliberation.
     *
     * @param itemList  List of Points
     * @return          Point in itemList closest to currentLocation
     */
    protected synchronized Point closestItem(Set<Point> itemList) {
        if(itemList.isEmpty()) return null;
        Iterator<Point> it = itemList.iterator();
        Point closest = it.next();
        double distance = closest.distance(currentLocation);
        while(it.hasNext()) {
            Point candidatePoint = it.next();
            if(candidatePoint.distance(currentLocation) < distance) {
                closest = candidatePoint;
                distance = candidatePoint.distance(currentLocation);
            }
        }
        return closest;
    }

    /**
     * Stringify the beliefs of this agent for presentation in the frontend
     * @return String list of beliefs
     */
    public ArrayList<String> getBeliefs() {
        ArrayList<String> beliefs = new ArrayList<>();
        beliefs.add(String.format("location(%dx%d)", this.currentLocation.x, this.currentLocation.y));
        return beliefs;
    }
}
