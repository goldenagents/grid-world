package agent.person;

import environment.Environment;
import environment.exceptions.ExternalActionFailedException;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.PlanToAgentInterface;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanExecutionError;

import java.awt.*;
import java.util.concurrent.TimeUnit;

/**
 * A plan to go to some specific coordinates on the grid. The plan is triggered once by a {@code GoToGoal} and
 * executed during each deliberation cycle thereafter until the goal has been achieved. During each deliberation cycle
 * (i.e. each execution of this plan), the agent senses its position in the environment, and tries to take a single step
 * towards the goal. If the action fails the path is likely blocked and a PlanExecutionError is thrown, which is
 * automatically adopted by the agent as an InternalTrigger, which triggers a Repair plan (in this case
 * {@code MoveRepairPlan}).
 *
 * This plan does not look ahead and just tries to move in the desired direction. It is left as an exercise to the reader
 * to make this plan more intelligent, by taking into account possible paths and blockages, and by planning ahead.
 */
public class GoToPlan extends Plan {

    public static int AFTER_MOVE_DELAY = 0;

    private Point destination;

    private Point currentLocation;
    private Environment env;
    private AgentID agentID;
    private GoToGoal goal;

    /**
     * Default constructor
     * @param goal The GoToGoal which triggered this plan, containing a destination
     */
    public GoToPlan(GoToGoal goal) {
        this.goal = goal;
        this.destination = goal.getDestination();
    }

    @Override
    public void execute(PlanToAgentInterface planToAgentInterface) throws PlanExecutionError {
        PersonContext context = planToAgentInterface.getContext(PersonContext.class);
        this.currentLocation = context.getCurrentLocation();
        this.env = context.getEnvironment();
        this.agentID = planToAgentInterface.getAgentID();

        MoveExecutionError.MOVE_DIRECTION direction = determineNextStep();

        try {
            context.setCurrentLocation(this.env.sensePosition(planToAgentInterface.getAgentID()));
            attemptMove(direction);
            context.setCurrentLocation(this.env.sensePosition(planToAgentInterface.getAgentID()));
        } catch (ExternalActionFailedException e) {
//            System.err.format("[GoToPlan - %s] - Tried to move %s from (%dx%d) towards (%dx%d). Failed due to '%s'\n",
//                    planToAgentInterface.getAgentID(),
//                    direction.toString(),
//                    context.getCurrentLocation().x, context.getCurrentLocation().y,
//                    destination.x, destination.y,
//                    e.getMessage());
//            System.err.flush();
            goal.setPursued(false);

            // The {@code MoveExecutionError} extends the {@code PlanExecutionError}, meaning this goal will not be
            // executed anymore until the MoveRepairPlan has been executed and signalled execution of this plan may
            // continue
            throw new MoveExecutionError(this.goal, direction);
        }

        if(context.getCurrentLocation().equals(this.destination)) {
            // The plan is finished when the destination is reached. After which, it should not be executed again.
            if(this.goal != null) this.goal.markAchieved();
            setFinished(true);
        }
    }

    /**
     * Pick one of four directions which will take the agent closer to its destination
     * @return  One of four directions to move towards
     */
    private MoveExecutionError.MOVE_DIRECTION determineNextStep() {
        MoveExecutionError.MOVE_DIRECTION direction = MoveExecutionError.MOVE_DIRECTION.INVALID;

        if (this.currentLocation.y > this.destination.y) {
            direction = MoveExecutionError.MOVE_DIRECTION.NORTH;
        } else if (this.currentLocation.x < this.destination.x) {
            direction = MoveExecutionError.MOVE_DIRECTION.EAST;
        } else if (this.currentLocation.y < this.destination.y) {
            direction = MoveExecutionError.MOVE_DIRECTION.SOUTH;
        } else if(this.currentLocation.x > this.destination.x) {
            direction = MoveExecutionError.MOVE_DIRECTION.WEST;
        }

        return direction;
    }

    /**
     * Attempt to move one step in the chosen direction
     * @param direction     A wind direction to move towards
     * @throws ExternalActionFailedException    If the environment prohibits moving a step in the specified direction
     */
    private void attemptMove(MoveExecutionError.MOVE_DIRECTION direction) throws ExternalActionFailedException {
        delay();
        switch(direction) {
            case EAST:
                this.env.east(this.agentID);
                break;
            case WEST:
                this.env.west(this.agentID);
                break;
            case NORTH:
                this.env.north(this.agentID);
                break;
            case SOUTH:
                this.env.south(this.agentID);
                break;
        }
    }

    private void delay() {
        if(GoToPlan.AFTER_MOVE_DELAY > 0) {
            try {
                TimeUnit.MILLISECONDS.sleep(GoToPlan.AFTER_MOVE_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
