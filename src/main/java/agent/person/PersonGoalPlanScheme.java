package agent.person;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Trigger;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanScheme;

/**
 * Trigger a GoToPlan if the agent has a goal to move to a certain set of coordinates.
 */
public class PersonGoalPlanScheme implements PlanScheme {
    @Override
    public Plan instantiate(Trigger trigger, AgentContextInterface agentContextInterface) {
        Plan plan = Plan.UNINSTANTIATED;

        if(trigger instanceof GoToGoal) {
            plan = new GoToPlan((GoToGoal) trigger);
        }

        if(trigger != null && plan != Plan.UNINSTANTIATED)
            plan.setPlanGoal(trigger);

        return plan;
    }
}
