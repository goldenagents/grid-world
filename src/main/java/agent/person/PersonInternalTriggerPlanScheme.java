package agent.person;

import org.uu.nl.net2apl.core.agent.AgentContextInterface;
import org.uu.nl.net2apl.core.agent.Trigger;
import org.uu.nl.net2apl.core.plan.Plan;
import org.uu.nl.net2apl.core.plan.PlanScheme;

/**
 * Triggers a repair plan if moving towards some coordinates failed
 */
public class PersonInternalTriggerPlanScheme implements PlanScheme {

    @Override
    public Plan instantiate(Trigger trigger, AgentContextInterface agentContextInterface) {
        Plan plan = Plan.UNINSTANTIATED;

        if(trigger instanceof MoveExecutionError) {
            plan = new MoveRepairPlan((MoveExecutionError) trigger);
        }

        if(trigger != null)
            plan.setPlanGoal(trigger);

        return plan;
    }
}
