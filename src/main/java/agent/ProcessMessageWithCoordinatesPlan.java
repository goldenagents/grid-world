package agent;

import org.uu.nl.net2apl.core.fipa.acl.ACLMessage;
import org.uu.nl.net2apl.core.plan.Plan;

import java.awt.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Since both Harry and Sally can receive messages in which coordinates are encoded as a string,
 * this abstract class handles the parsing of coordinates from a string message. Both Harry and Sally
 * extend this plan to extract the information from messages to their specific needs.
 */
public abstract class ProcessMessageWithCoordinatesPlan extends Plan {

    protected static final String encodedCoordinatePattern = "\\(\\s*(\\d+)\\s*[,;]\\s*(\\d+)\\s*\\)";

    protected ACLMessage message;

    public ProcessMessageWithCoordinatesPlan(ACLMessage message) {
        this.message = message;
    }

    protected Point getCoordinates(String message, Pattern pattern) {
        Matcher matcher = pattern.matcher(message);
        if(matcher.matches()) {
            return new Point(
                Integer.parseInt(matcher.group(1)),
                Integer.parseInt(matcher.group(2)));
        } else {
            return null;
        }
    }
}
