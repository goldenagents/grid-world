import agent.harry.HarryContext;
import agent.harry.planscheme.HarryGoalPlanScheme;
import agent.harry.planscheme.HarryMessagePlanScheme;
import agent.person.GoToPlan;
import agent.person.PersonContext;
import agent.person.PersonGoalPlanScheme;
import agent.person.PersonInternalTriggerPlanScheme;
import agent.sally.SallyContext;
import agent.sally.goal.SearchGridWorldGoal;
import agent.sally.planscheme.SallyGoalPlanScheme;
import agent.sally.planscheme.SallyMessagePlanScheme;
import agent.simplemovingagent.SimpleMovingAgentGoalPlanScheme;
import agent.simplemovingagent.SimpleMovingGoal;
import environment.AgentInterface;
import environment.Environment;
import environment.exceptions.ExternalActionFailedException;
import environment.view.AgentColor;
import org.uu.nl.net2apl.core.agent.Agent;
import org.uu.nl.net2apl.core.agent.AgentArguments;
import org.uu.nl.net2apl.core.agent.AgentID;
import org.uu.nl.net2apl.core.agent.Goal;
import org.uu.nl.net2apl.core.fipa.FIPAMessenger;
import org.uu.nl.net2apl.core.platform.Platform;

import java.awt.*;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * The main class instantiates a Grid World environment and populates it with BDI agents.
 * The Grid World environment is a two dimensional grid, on which bombs, walls, and bomb traps can occur.
 * The agents are tasked with finding bombs and bringing them to bomb traps. Agents have a certain sense radius within
 * the grid, can only pickup a bomb if they are in the same location as the bomb and aren't currently carrying a bomb,
 * can only drop a bomb if they are in the same location as a bomb trap and are carrying a bomb and can't move through
 * walls or other agents.
 *
 * In this demo, two types of agents exist: Harry and Sally. Harry does not sense anything, but only brings bombs to
 * bomb traps. Sally does not carry any bombs, but constantly scouts the world for bombs and bomb traps, informing Harry
 * of every discovery.
 */
public class Main {

    // Starting points for Harry and Sally Agents
    private static final Point HARRY_START_POSITION = new Point(0,0);
    private static final Point SALLY_START_POSITION = new Point(8, 8);

    // Agents may move a bit fast. To show them moving over the grid, delay for this number of milliseconds (0 means
    // no delay)
    private static final int AFTER_MOVE_DELAY = 120;

    private static int nHarries = 1;
    private static int nSallies = 1;

    private static final int ENV_WIDTH = 15;
    private static final int ENV_HEIGHT = 15;

    // For simulation purposes, a headless environment can be instantiated, meaning no view is created
    private static final boolean INSTANTIATE_ENVIRONMENT_HEADLESS = false;

    private static Platform platform;
    private static Environment environment;

    public static ArrayList<AgentInterface> agents = new ArrayList<>();

    public static void main(String[] args) {
        setup();
        initiateWithHarryAndSally();

        // #Uncomment next line to test walking agent
        // addSimpleMovingAgent(platform, environment);
    }

    /**
     * Creates an environment and 2APL platform
     */
    private static void setup() {
        FIPAMessenger messenger = new FIPAMessenger();

        Main.platform = Platform.newPlatform(4, messenger);

        Main.environment = new Environment(
                new Dimension(Main.ENV_WIDTH, Main.ENV_HEIGHT),
                INSTANTIATE_ENVIRONMENT_HEADLESS);

        GoToPlan.AFTER_MOVE_DELAY = Main.AFTER_MOVE_DELAY;
    }

    /**
     * Creates the Harry and Sally agents, based on set parameters, and puts them on the 2APL platform and places them
     * in the environment
     */
    private static void initiateWithHarryAndSally() {
        for(int i = 0; i < Main.nHarries; i++) {
            Point startingPosition = Main.nHarries <= 1 ?
                    Main.HARRY_START_POSITION : Main.environment.getFirstFree(Main.HARRY_START_POSITION);

            if(startingPosition == null) {
                System.err.format("No more free cells in environment. Can't add %dth Harry agent.\n", i);
                return;
            }

            // Harry does not need to be instantiated with a CleanWorldGoal, as the goal is adopted automatically
            // after the first message from Sally
            Main.createAgent(
                    getHarryArguments(startingPosition),
                    startingPosition,
                    AgentColor.BLUE,
                    Main.nHarries > 1 ? String.format("Harry-%d", i) : "Harry"
            );
        }

        for(int i = 0; i < Main.nSallies; i++) {
            Point startingPosition = Main.nSallies <= 1 ?
                    Main.SALLY_START_POSITION : Main.environment.getFirstFree(Main.SALLY_START_POSITION);
            if(startingPosition == null) {
                System.err.format("No more free cells in environment. Can't add %dth Sally agent.\n", i);
                return;
            }
            Main.createAgent(
                    getSallyArguments(startingPosition),
                    startingPosition,
                    AgentColor.RED,
                    Main.nSallies > 1 ? String.format("Sally-%d", i) : "Sally",
                    new SearchGridWorldGoal()
            );
        }
    }

    /**
     * A simple moving agent can be used to demonstrate how agents may move through the environment without doing
     * anything else
     */
    private static void initiateWithSimpleMovingAgent() {
        Main.createAgent(
                getSimpleMovingAgentArguments(new Point(0,0)),
                new Point(0,0),
                AgentColor.ORANGE,
                "SimpleMoving",
                getSimpleMovingGoal()
        );
    }

    /**
     * A goal is used to activate a SimpleMovingAgent. This particular goal makes the SimpleMovingAgent move over
     * each cell in the grid iteratively
     *
     * @return  SimpleMovingGoal    A goal for the agent to move to every cell of the grid iteratively
     */
    private static SimpleMovingGoal getSimpleMovingGoal() {
        Point[] pointsToVisit = new Point[Main.ENV_WIDTH * Main.ENV_HEIGHT];
        int index = 0;
        for(int i = 0; i < Main.ENV_WIDTH; i++) {
            for(int j = 0; j < Main.ENV_HEIGHT; j++) {
                pointsToVisit[index] = new Point(i, j);
                index++;
            }
        }

        return new SimpleMovingGoal(pointsToVisit);
    }

    /**
     * Create a 2APL
     *
     * @param arguments             AgentArguments to use to instantiate a new agent
     * @param startingPosition      The coordinates the agent spawned at in the environment (must be empty)
     * @param color                 The color with which this agent will show up in the interface
     * @param localShortName        A short identifier used for logging
     * @param initialGoals          An initial set of goals for the agent to pursue (may be empty)
     * @return                      Agent instantiated with the passed parameters
     */
    private static boolean createAgent(AgentArguments arguments, Point startingPosition, AgentColor color, String localShortName, Goal... initialGoals) {
        arguments.addGoalPlanScheme(new PersonGoalPlanScheme());
        arguments.addInternalTriggerPlanScheme(new PersonInternalTriggerPlanScheme());

        Agent agent;

        try {
            agent = new Agent(platform, arguments);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return false;
        }

        for(Goal g : initialGoals) {
            agent.adoptGoal(g);
        }

        updateAgentIdWithLocalName(agent, localShortName);

        return enter(agent,startingPosition, color, localShortName);
    }

    private static void updateAgentIdWithLocalName(Agent agent, String localShortName) {
//        try {
//            agent.getAID().setName(localShortName);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
    }

    /**
     * Create an interfaced agent which can be added to the grid world environment. Add the agent to the environment
     * @param agent             2APL agent object to add to the environment
     * @param start             Start position where the agent should enter the environment
     * @param color             Agent display color in the GUI
     * @param shortLocalName    A short local name used to identify this agent in the GUI
     */
    private static boolean enter(Agent agent, Point start, AgentColor color, String shortLocalName) {
        APLToEnvAgent interfacedAgent = new APLToEnvAgent(agent, shortLocalName);
        Main.agents.add(interfacedAgent);

        try {
            environment.enter(interfacedAgent, start, color);
            return true;
        } catch(ExternalActionFailedException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * An agent is instantiated using Agent Arguments. The arguments allow adding plans, beliefs, goals and plan schemes
     * to the agent. A plan scheme uses beliefs to instantiate a plan based on an active goal during each deliberation
     * cycle. Beliefs are added in the form of Contexts, which are generic Java objects on which all relevant beliefs
     * can be encoded as class members
     *
     * Harry's belief base consists of locations of bombs, bomb traps, and walls of which it is informed by Sally, plus
     * a belief for its current location. It has plans to find closest bombs or closest traps, or to pick up or drop a
     * bomb. The belief base triggers the right plan dependent on the current situation of Harry (e.g. where is it,
     * is it carrying any bombs).
     *
     * A message plan scheme is added to initiate a plan when Harry receives a message from Sally. This plan processes
     * the message (which contains locations for bombs or bomb traps).
     *
     * @param startingPosition  Harry's starting position, which will be set as a belief for Harry
     *
     * @return Agent Arguments with which Harry can be instantiated.
     */
    private static AgentArguments getHarryArguments(Point startingPosition) {
        AgentArguments arguments = new AgentArguments();
        HarryContext context = new HarryContext(Main.environment);
        context.setCurrentLocation(startingPosition);
        arguments.addContext(context, HarryContext.class, PersonContext.class);
        arguments.addGoalPlanScheme(new HarryGoalPlanScheme());
        arguments.addMessagePlanScheme(new HarryMessagePlanScheme());

        return arguments;
    }

    /**
     * Sally's belief base consists of locations of bombs, bomb traps and walls of which she has already informed Harry.
     * Every time she sends a message to Harry, these beliefs are updated. Beliefs are removed when Harry informs her
     * he picked up a bomb, or when Harry informs her there is no longer a bomb at that position.
     *
     * Sally has plans to roam the Grid World randomly, continuously scouting the environment for bombs, bomb traps, and
     * walls. Every time she encounters one of those she sends a message to Harry with its position. Sally has a constant
     * maintainance goal (i.e. never achieved but constantly pursued) to continue scouting the Grid World, which triggers
     * the previously described plan using the plan schemes.
     *
     * The message plan scheme is added to initiate a plan when Sally receives a message from Harry, informing her of
     * the removal of a bomb or a trap from the environment. This plan scheme triggers a plan to process that message
     * and update the belief base accordingly.
     *
     * @param startingPosition  Sally's starting position, which will be set as a belief for Sally
     *
     * @return Agent Arguments with which Sally can be instantiated.
     */
    private static AgentArguments getSallyArguments(Point startingPosition) {
        AgentArguments arguments = new AgentArguments();
        SallyContext context = new SallyContext(Main.environment);
        context.setCurrentLocation(startingPosition);
        Main.agents.forEach(x -> context.addAgent((AgentID) x.getAgentIdentifier()));
        arguments.addContext(context, SallyContext.class, PersonContext.class);
        arguments.addGoalPlanScheme(new SallyGoalPlanScheme());
        arguments.addMessagePlanScheme(new SallyMessagePlanScheme());

        return arguments;
    }

    /**
     * Agent arguments containing a simple plan and plan scheme to visit a certain set of cells in the Grid World
     *
     * @param startingPosition The starting position of this agent
     * @return                 Agent arguments with which a SimpleMovingAgent can be instantiated
     */
    private static AgentArguments getSimpleMovingAgentArguments(Point startingPosition) {
        AgentArguments args = new AgentArguments();
        args.addGoalPlanScheme(new SimpleMovingAgentGoalPlanScheme());
        args.addContext(new PersonContext(Main.environment, new Point(0,0)), PersonContext.class);
        return args;
    }
}
